import React, { useState } from "react";
import ListProduct from "./ListProduct";
import data from "./Data.json";

const BTShoe = () => {
  const [prdDetail,setPrdDetail] = useState (data[0])
  console.log('prddetail',prdDetail)
  const handlePrdDetail = (product) => {
    setPrdDetail(product)
  }
  return (
    <div>
      <h1 className="text-center">Shoe shop</h1>
      <ListProduct handlePrdDetail = {handlePrdDetail} data={data} />

      {/* modal  */}

      <div>
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Chi tiết sản phẩm
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-4">
                    <img className="img-fluid" src= {prdDetail.image} />
                  </div>
                  <div className="col-8">
                    <p className="font-weight-bold">{prdDetail.name}</p>
                    <p className="mt-3">{prdDetail.description}</p>
                    <p className="font-weight-bold mt-3">{prdDetail.price}$</p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BTShoe;
