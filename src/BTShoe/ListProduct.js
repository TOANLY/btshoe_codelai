
import React from 'react'
import Item from './Item'

const ListProduct = (props) => {
  const {data, handlePrdDetail} = props
  return (
    <div className='row'>
        {data.map((item,index) => {
          return <Item handlePrdDetail = {handlePrdDetail} product = {item} key = {item.id}/>
        }) }
        
    </div>
  )
}

export default ListProduct