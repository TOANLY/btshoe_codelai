import React from "react";

const Item = (props) => {
  const { product, handlePrdDetail } = props;
  console.log("handle", handlePrdDetail);
  return (
    <div className="col-4">
      <div className="card">
        <img src={product.image} />
        <div className="card-body">
          <h4 className="name">{product.name}</h4>
          <p>{product.description}</p>
          <h5 className="mt-3">{product.price}$</h5>
          <button
            className="btn btn-success"
            data-toggle="modal"
            data-target="#exampleModal"
            onClick={() => handlePrdDetail(product)}
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
};

export default Item;
